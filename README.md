Twin City Security, LLC is a certified Contract Security Company as established by the Alabama Security Regulatory Board. We have provided bonded unarmed and armed security services for private businesses and government agencies in Alabama since 1975.

Address: 2200 Executive Park Drive, Opelika, AL 36801

Phone: 334-749-8385